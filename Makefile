ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif

all: asciinauta
asciinauta.o: asciinauta.c
clean: 
	rm asciinauta *.o
install: asciinauta
	install -m 755 asciinauta $(PREFIX)/bin/
run: asciinauta
	./asciinauta
