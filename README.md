# Asciinaut

This is my attempt at a game for [librejam](https://leagueh.xyz/en/jam.html)
It's very minimal and unfinished. Basically a dodging game.

The idea was to create a game similar to "Ridiculous fishing" where an astronaut would have to go "down" looking for the parts of a spaceship that crashed.

## Sample video

![](asciinaut.webm)

## Controls

Use h (left)  and l (right) to move
