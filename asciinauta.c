#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <termios.h>
#include <unistd.h>

// Terminal Settings
#define NB_ENABLE 0x01
#define NB_DISABLE 0x00

// ANSI Control Codes
#define CLEAR "\e[2J"
#define HOME "\e[H"
#define HIDE_CURSOR "\e[?25l"
#define SHOW_CURSOR "\e[?12l\e[?25h"

// ANSI Colors
#define FRED "\e[0;31m"
#define BBLACK "\e[0;40m"
#define FWHITE "\e[0;37m"

// Game Settings
#define MAX_METEORITES 100
#define STARS_PER_LAYER 200
#define PARALAX_LAYERS 4
#define MAX_WIDTH 20
#define MAX_HEIGHT 20
#define FPS 16666

int kbhit() {
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    select(STDIN_FILENO + 1, &fds, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &fds);
}

void nonblock(int state) {
    struct termios ttystate;

    // get the terminal state
    tcgetattr(STDIN_FILENO, &ttystate);

    if (state == NB_ENABLE) {
        // turn off canonical mode
        ttystate.c_lflag &= ~ICANON;
        ttystate.c_lflag &= ~ECHO;
        // minimum of number input read.
        ttystate.c_cc[VMIN] = 1;
    } else if (state == NB_DISABLE) {
        // turn on canonical mode
        ttystate.c_lflag |= ICANON;
        ttystate.c_lflag |= ECHO;
    }
    // set the terminal attributes.
    tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}

void initTerminal() {
    printf(CLEAR);
    printf(HIDE_CURSOR);
    printf(BBLACK);
    nonblock(NB_ENABLE);
}

void restoreTerminal() {
    nonblock(NB_DISABLE);
    printf(SHOW_CURSOR);
}

struct star {
    int x;
    int y;
    int speed;
};

struct meteorite {
    int x;
    int y;
    int speedX;
    int speedY;
};

enum color { WHITE, RED };
struct cell {
    char cell;
    enum color c;
};

struct player {
    int x;
    int y;
    int lives;
    int hit;
    int hDelta;
};

struct gameState {};

void drawCanvas(struct cell canvas[MAX_HEIGHT][MAX_WIDTH]) {
    for (int i = 0; i < MAX_HEIGHT; i++) {
        for (int j = 0; j < MAX_WIDTH; j++) {
            if (canvas[i][j].c == WHITE) {
                printf(FWHITE);
            } else
                printf(FRED);
            printf("%c", canvas[i][j].cell);
        }
        printf("\n");
    }
}

void clearCanvas(struct cell canvas[MAX_HEIGHT][MAX_WIDTH]) {
    for (int i = 0; i < MAX_HEIGHT; i++) {
        for (int j = 0; j < MAX_WIDTH; j++) {
            canvas[i][j].cell = ' ';
            canvas[i][j].c = 0;
        }
    }
}

void spawnMeteorites(struct meteorite meteorites[MAX_METEORITES]) {
    for (int nMeteorites = 0; nMeteorites < MAX_METEORITES; nMeteorites++) {
        int meteoriteI = MAX_HEIGHT + (rand() % (MAX_HEIGHT * 20));
        int meteoriteJ = rand() % (MAX_WIDTH - 1);

        meteorites[nMeteorites].x = meteoriteI;
        meteorites[nMeteorites].y = meteoriteJ;
        meteorites[nMeteorites].speedX = 3 + (rand() % PARALAX_LAYERS);
    }
}

void spawnStars(struct star stars[PARALAX_LAYERS * STARS_PER_LAYER]) {
    for (int nStars = 0; nStars < STARS_PER_LAYER * PARALAX_LAYERS; nStars++) {
        int starI = rand() % (MAX_HEIGHT * 20);
        int starJ = rand() % (MAX_WIDTH - 1);

        stars[nStars].x = starI;
        stars[nStars].y = starJ;
        stars[nStars].speed = rand() % PARALAX_LAYERS;
    }
}

void updateStars(struct cell canvas[MAX_HEIGHT][MAX_WIDTH],
                 struct star stars[PARALAX_LAYERS * STARS_PER_LAYER],
                 int delta) {
    for (int si = 0; si < PARALAX_LAYERS * STARS_PER_LAYER; si++) {
        int i = stars[si].x;
        int j = stars[si].y;
        int speed = stars[si].speed;

        if (speed > 0) {
            if (delta % (speed * 100) == 0) {
                --stars[si].x;
            }
        }
        if (i < MAX_HEIGHT && j < MAX_WIDTH && i > 0 && j > 0)
            canvas[i][j].cell = '.';
    }
}

void updateMeteorites(struct cell canvas[MAX_HEIGHT][MAX_WIDTH],
                      struct meteorite meteorites[MAX_METEORITES], int delta) {

    for (int mi = 0; mi < MAX_METEORITES; mi++) {
        int i = meteorites[mi].x;
        int j = meteorites[mi].y;
        int speedX = meteorites[mi].speedX;

        if (speedX > 0) {
            if (delta % (speedX * 3) == 0) {
                --meteorites[mi].x;
            }
        }
        if (i < MAX_HEIGHT && j < MAX_WIDTH && i > 0 && j > 0) {
            canvas[i][j].cell = '#';
            canvas[i][j - 1].cell = '{';
            canvas[i][j + 1].cell = '}';
        }
    }
}

void updatePlayer(struct cell canvas[MAX_HEIGHT][MAX_WIDTH], struct player *p,
                  int delta) {

    // wobble up and down
    if (delta % 300 == 0) {
        p->x--;
    } else if (delta % 200 == 0) {
        p->x++;
    }

    // rope
    for (int i = 0; i < (MAX_HEIGHT / 2)-1; i++) {
        canvas[i][p->y+2].cell = '|';
    }

    // player
    canvas[p->x -1][p->y+2].cell = '.';
    canvas[p->x][p->y].cell = '@';
    canvas[p->x][p->y+1].cell = '/';
    canvas[p->x + 1][p->y].cell = 'H';
    canvas[p->x + 1][p->y - 1].cell = '/';
    canvas[p->x + 1][p->y + 1].cell = ' ';
    canvas[p->x + 2][p->y - 1].cell = '/';
    canvas[p->x + 2][p->y + 1].cell = '\\';


    int r = 1 + (rand() % 3);
    if (delta % (150) < (40)) {
        canvas[p->x + 1][p->y - 1].cell = '/';
        canvas[p->x][p->y - 1].cell = ' ';
    } else {
        canvas[p->x + 1][p->y - 1].cell = ' ';
        canvas[p->x][p->y - 1].cell = '\\';
    }

}

void handleCollision(struct cell canvas[MAX_HEIGHT][MAX_WIDTH],
                     struct player *p, int delta) {
    int px = p->x;
    int py = p->y;
    if (!p->hit &&
            ( canvas[px][py].cell != '@'||
             canvas[px][py+1].cell != '/'||
             (canvas[px][py-1].cell != '\\' && canvas[px][py-1].cell != ' ') ||
             canvas[px + 1][py].cell != 'H'||
             (canvas[px + 1][py - 1].cell != '/'&& canvas[px + 1][py - 1].cell != ' ')||
             canvas[px + 1][py + 1].cell != ' '||
             canvas[px + 2][py - 1].cell != '/' ||
             canvas[px + 2][py + 1].cell != '\\')) {
        p->lives--;
        p->hit = 1;
        p->hDelta = delta;
    }

    if (p->hit) {
        canvas[px][py+1].c = p->hit;
        canvas[px][py-1].c = p->hit;
        canvas[px][py].c = p->hit;
        canvas[px + 1][py].c = p->hit;
        canvas[px + 1][py - 1].c = p->hit;
        canvas[px + 1][py + 1].c = p->hit;
        canvas[px + 2][py - 1].c = p->hit;
        canvas[px + 2][py + 1].c = p->hit;
    }
}

void update(struct cell canvas[MAX_HEIGHT][MAX_WIDTH],
            struct star stars[PARALAX_LAYERS * STARS_PER_LAYER],
            struct meteorite meteorites[MAX_METEORITES], struct player *p,
            int delta) {
    clearCanvas(canvas);

    if (p->hit && delta - p->hDelta > 50)
        p->hit = 0;

    updateStars(canvas, stars, delta);
    updatePlayer(canvas, p, delta);
    updateMeteorites(canvas, meteorites, delta);
    handleCollision(canvas, p, delta);
    updatePlayer(canvas, p, delta); // draw the player last
}

void draw(struct cell canvas[MAX_HEIGHT][MAX_WIDTH], struct player p,
          int delta) {
    printf(HOME);
    printf("\n");
    printf("Lives: ");
    for (int l = 0; l < 3; l++) {
        if (l < p.lives) printf("* ");
        else printf(" ");
    }
    printf("\n");
    drawCanvas(canvas);

    fprintf(stderr, "Distance: %d\n", delta/50);
    fprintf(stderr, "Lives: %d\n", p.lives);
}

int handleInput(struct player *p) {
    int k = kbhit();
    if (k != 0) {
        char key = fgetc(stdin);
        switch (key) {
        case 'q':
            k = 1;
            break;
        case 'h':
            k = 0;
            if (p->y >= 3)
                p->y -= 1;
            break;
        case 'l':
            k = 0;
            if (p->y < MAX_WIDTH - 3)
                p->y += 1;
            break;
        case 'p':
            k = 0;

            break;
        default:
            k = 0;
            break;
        }
    }
    return k;
}

int main() {
    int quit = 0;
    long delta = 0;

    struct cell canvas[MAX_HEIGHT][MAX_WIDTH];
    struct player p = {10, 5, 3};

    struct star stars[PARALAX_LAYERS * STARS_PER_LAYER];
    spawnStars(stars);
    struct meteorite meteorites[MAX_METEORITES];
    spawnMeteorites(meteorites);

    initTerminal();
    while (!quit) {
        quit = handleInput(&p);
        update(canvas, stars, meteorites, &p, delta);
        draw(canvas, p, delta);
        if (p.lives <= 0)
            quit = 1;

        delta += 1;
        usleep(FPS); // each frame is (1/60)s
    }
    restoreTerminal();
    return 0;
}
